#!/usr/bin/env python3

# spielaffe.de siterip
# Use logerr(), loginfo(), logverb() and logdebug() for logging
# Games get loaded into "<working dir>/flash"

# by terorie, 2018
# pls don't steal

import requests
import lxml.html
import os
import re
import shutil
import argparse
import logging
import base64


# Args
parser = argparse.ArgumentParser()
parser.add_argument('-v', '--verbose', help="More info", action="store_true")
parser.add_argument('-d', '--debug', help="Debugging info", action="store_true")
parser.add_argument('-dd','--spammy', help="Extremely much debugging info", action="store_true")
parser.add_argument('start', nargs='?', type=int, help="start page", default=0)
args = parser.parse_args()

# Logging boilerplate
# Log to either debug, verbose or info
logging.addLevelName(logging.CRITICAL, "\033[1;33mWARN\033[1;0m")
logging.addLevelName(logging.ERROR, "\033[1;92mINFO\033[1;0m")
logging.addLevelName(logging.WARNING, "\033[1;35mVERB\033[1;0m")
logging.addLevelName(logging.INFO, " DBG")
log = logging.getLogger()
handler = logging.StreamHandler()
formatter = logging.Formatter('%(levelname)-8s %(message)s')
handler.setFormatter(formatter)
log.addHandler(handler)
if args.spammy: log.setLevel(logging.DEBUG)
elif args.debug: log.setLevel(logging.INFO)
elif args.verbose: log.setLevel(logging.WARNING)
else: log.setLevel(logging.ERROR)
logerror = log.critical
loginfo = log.error
logverbose = log.warning
logdebug = log.info


# Regex for finding .swfs
SWF_REGEX = '(https?:)?//[\w\d\._\-%]+/([\w\d_\-]+/)+[\w\d_\-]+\.swf'

# Connection keep-alive
session = requests.Session()

def get_page_url(page):
	if page == 1:
		return "http://www.spielaffe.de/Neue-Spiele"
	else:
		return "http://www.spielaffe.de/Neue-Spiele/" + str(page)

def get_game_dir(game_id):
	letter = game_id[0].upper()
	if letter < 'A' or letter > 'Z':
		letter = '#'
	return os.path.join("flash", str(letter), game_id)

# Tries to extract a .swf URL from a page
# Returns False if the page doesn't point to a flash game
# Returns None if .swf URL unknown
# Returns the URL on success
def try_extract_url(swf_page):
	# Default path search
	m = re.search(SWF_REGEX, swf_page)
	if m is not None:
		swf_url = m.group()

		# Blacklist
		if swf_url.endswith('gameapi.swf'):
			return None
		elif swf_url.endswith('expressInstall.swf'):
			return None
		elif swf_url.endswith('preloader.swf'):
			return None

		if swf_url.startswith("//"):
			swf_url = "http:" + swf_url
		logverbose(" ~ Detected link: %s", swf_url)
		return swf_url

	# iframed
	m = re.search("var isGameLoaded = false;\\nfunction loadGameFile\\(\\) {\\nif \\(isGameLoaded\\) {\\nreturn true;\\n}var url = '(.*[^\\\\])';", swf_page)
	if m is not None:
		# URL of iframe
		i_url = m.group(1)

		# These serve only JS games
		if i_url.startswith('//games.poki.com/'):
			return False
		if i_url.startswith('//html5.gamedistribution.com/'):
			return False

		# 'html5' or 'webgl' in URL
		if i_url.find('html5') != -1 or i_url.find('webgl') != -1:
			logdebug(" - Embed is probably not flash (URL): %s", i_url)
			return False

		if i_url.startswith('//'):
			i_url = 'http:' + i_url

		try:
			i_r = session.get(i_url, timeout=2, headers={'accept': 'text/html'})
		except:
			logerror(" ~ Failed to get embed page: %sx", i_url)
			return None

		# silvergames.com: swf in base64
		if i_url.startswith('http://www.silvergames.com/'):
			try:
				# Parse HTML
				i_doc = lxml.html.fromstring(i_r.text)
				i_enc_link = i_doc.get_element_by_id('game_embed')
				i_url_base64 = i_enc_link.get('data-src')
				i_url = base64.b64decode(i_url_base64).decode('utf-8')
				logverbose(' ~ Decoded URL: %s', i_url)
				return i_url
			except Exception as e:
				logerror(' ! silvergames.com extraction failed: %s', e)
				return None

		# Has canvas
		if i_r.text.find('</canvas') != -1:
			return False

		# Unity game
		if i_r.text.find('UnityLoader.js') != -1:
			return False

		# Find swf
		i_m = re.search(SWF_REGEX, i_r.text)
		if i_m is not None:
			swf_url = i_m.group()
			if swf_url.startswith("//"):
				swf_url = "http:" + swf_url
			logverbose(" ~ Detected link in embed: %s", swf_url)
			return swf_url

		# Heuristics on content
		if i_r.text.find('game.js') != -1:
			logdebug(" - Embed is probably not flash (js): %s", i_url)
			return False
		if i_r.text.find('UnityLoader.js') != -1:
			logdebug(" - Embed is not flash (Unity): %s", i_url)
			return False
		if re.search('three(\.min)?\.js', i_r.text) is not None:
			logdebug(" - Embed is not flash (three.js): %s", i_url)
			return False
		if i_r.text.find('"//www.box10.com/downloads/html5/') != -1:
			logdebug(" - Embed is not flash (box10 embed): %s", i_url)
			return False

		# No swf found
		logverbose(" ~ No swf found in embed: %s", i_url)
		return None

	# Heuristics
	m = re.search('\w.+\.swf', swf_page)
	if m is not None:
		res = m.group()

		logverbose(" ~ Probably swf: %s", res)
		return None

	# Failed
	return None

page = args.start
while True:
	page += 1
	page_url = get_page_url(page)

	r = session.get(page_url, allow_redirects=False)

	# Check for HTTP error code
	if r.status_code != 200:
		logerror("Status %d for %s", r.status_code, page_url)
		break

	# Parse HTML
	doc = lxml.html.fromstring(r.text)

	# Find all game containers
	games = doc.find_class('gameNameContainer')

	del r
	del doc

	new_game_count = 0
	failed_count = len(games)
	not_flash_count = 0

	# Extract links
	for container in games:
		game_a = container.find_class('gameLink')[0]
		game_href = game_a.get('href')
		# Get last part of URL
		_, _, game_id = game_href.rpartition('/')

		game_ldir = get_game_dir(game_id)

		# Already downloaded
		if os.path.exists(game_ldir):
			logdebug(' ~ Already downloaded "%s"', game_id)
			failed_count -= 1
			continue

		# Download afterload script
		game_url = "http://www.spielaffe.de/game/remote_afterload_special/%s/intermediate_special" % game_id
		game_r = session.get(game_url)

		# Check for HTTP error code
		if game_r.status_code != 200:
			logerror("Status %d for %s" % (game_r.status_code, game_url))
			continue

		# Try getting an .swf URL from the afterload
		swf_url = try_extract_url(game_r.text)
		del game_r

		# No .swf found
		if swf_url is False:
			failed_count -= 1
			not_flash_count += 1
			continue
		if swf_url is None:
			continue

		# Get game name
		_, _, swf_name = swf_url.rpartition('/')

		# Download
		try:
			swf_r = session.get(swf_url, stream=True, timeout=5)
		except:
			logerror("Failed to download swf: %s", swf_url)
			continue

		# Create dir if necessary
		if not os.path.exists(game_ldir):
			os.makedirs(game_ldir)

		with open(os.path.join(game_ldir, swf_name), 'wb') as swf_file:
			shutil.copyfileobj(swf_r.raw, swf_file)
		del swf_r

		# Log
		loginfo(' + Got "%s"' % game_id)

		# Success
		new_game_count += 1
		failed_count -= 1

	loginfo("Processed page %03d:\t%02d games, %02d new games, %02d failed extractions, %02d not flash"
		% (page, len(games), new_game_count, failed_count, not_flash_count))

loginfo("Done.")
